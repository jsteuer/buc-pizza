package de.htwsaar.buc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.htwsaar.buc.pizza.DummyPizzaDatabase;
import de.htwsaar.buc.pizza.PizzaDatabase;
import de.htwsaar.buc.pizza.PriceCalculator;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 *
 * @author jsteuer
 */
public class PaymentJavaDelegate implements JavaDelegate {

	private final static Logger LOGGER = Logger.getLogger(PaymentJavaDelegate.class.getSimpleName());

	private final String apiEndpoint;

	public PaymentJavaDelegate() {
		// add in setenv.sh: -DBEZAHL_SERVICE_URL=http://localhost:3000/api/orchestrator/start 
		apiEndpoint = System.getProperty("BEZAHL_SERVICE_URL"); // z.B. "http://localhost:3000/api/orchestrator/start"
	}

	@Override
	public void execute(DelegateExecution de) throws Exception {
		// collect infos
		String pizzaIdsAsString = (String) de.getVariable("pizzaIds");
		List<Integer> pizzaIds = Arrays.stream(pizzaIdsAsString.split(","))
				.map(Integer::parseInt)
				.collect(Collectors.toList());
		String customerId = (String) de.getVariable("paymentInfo.customerId");
		String discountCode = (String) de.getVariable("paymentInfo.discountCode");
		String pin = (String) de.getVariable("paymentInfo.pin");

		// calculate the price
		PizzaDatabase pizzaDatabase = new DummyPizzaDatabase();
		PriceCalculator calculator = new PriceCalculator(pizzaDatabase);
		int priceCents = calculator.calculatePrice(pizzaIds);

		// build request payload
		ObjectNode requestPayload = new ObjectMapper().createObjectNode();
		{
			requestPayload.put("Name", "Payment_OS");
			ObjectNode inputNode = requestPayload.putObject("Input");
			{
				inputNode.put("cid", customerId);
				inputNode.put("pin", pin);
				inputNode.put("discountCode", discountCode);
				inputNode.put("price", (float) (((double) priceCents) / 100.0));
				inputNode.put("orderCount", 1);
			}
		}
		// perform the request
		Client client = ClientBuilder.newClient();
		final ObjectNode response;
		try {
			response = client.target(apiEndpoint)
					.request(MediaType.APPLICATION_JSON)
					.post(Entity.json(requestPayload), ObjectNode.class);
		} catch (Exception e) {
			throw new RuntimeException("request to payment service '" + apiEndpoint + "' failed", e);
		}
		boolean isPayed = response.get("result").asBoolean();
		String message = response.get("message").asText();
		if (isPayed) {
			LOGGER.info("\t" + "PAYMENT SUCCESS: " + message);
		} else {
			LOGGER.info("\t" + "PAYMENT FAILED: " + message);
		}
		de.setVariable("isPayed", isPayed);
	}

}
