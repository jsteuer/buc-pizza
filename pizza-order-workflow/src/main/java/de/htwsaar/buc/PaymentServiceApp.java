package de.htwsaar.buc;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("Bezahl Service App")
public class PaymentServiceApp extends ServletProcessApplication {
  // empty implementation
}
