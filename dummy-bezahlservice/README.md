# Dummy Bezahlservice

## Anforderungen
- nodejs (https://nodejs.org/en/)

## Benutzen des Servcices

    npm install         // installiert Abhängigkeiten, nur einmal notwendig
    npm start           // startet den Service


Anschließend kann man unter `http://localhost:3000/` die Admin-Seite einsehen, die eine Übersicht
über alle Konten liefert. Bezahlungen können wie folgt ausgeführt werden:

    curl 'http://localhost:3000/pay' \
            -H 'content-type: application/json' \
            --data-binary '{"kundenNummer":1,"betrag":5.5,"pin":1111}' \
            --compressed
            
Bei Erfolg gibt der Service folgende Daten zurück:

    {status: "ok", message: "<Infotext hier>"}

Bei Misserfolg folgende:

    {status: "error", message: "<Fehlermeldung hier>"}
