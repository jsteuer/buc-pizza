let express = require("express");
let bodyParser = require("body-parser");

let app = express();
app.use(bodyParser.json());

let konten = [
	{
		cid: "jsteuer",
		budget: 14.50,
		pin: "1111"
	}, {
		cid: "jdillenkofer",
		budget: 15.50,
		pin: "2222"
	}, {
		cid: "rbothen",
		budget: 27.80,
		pin: "3333"
	}
];

app.get('/', function (req, res) {
	res.send(`
        <html>
                <head>
                    <title></title>
                    <meta content="">
                    <style>
                        table, th, td {
                            border: 1px solid black;
                            border-collapse: collapse;
                        }
                    </style>
                </head>
                <body>
                    <h1>Supertoller Bezahlservice</h1>
                    <h2>Admin-Bereich</h2>
                    <table>
                        <tr>
                            <th>CustomerID</th>
                            <th>Kontostand</th> 
                            <th>Pin</th>
                        </tr>
                        ${konten.map(k => `
                        <tr>
                            <td>${k.cid}</td>
                            <td>${k.budget}€</td> 
                            <td>${k.pin}</td>
                        </tr>
                        `)
			.join("")}           
                    </table>
                </body>
            </html>
        `);
});

app.post("/api/orchestrator/start", function (req, res) {
	let json = req.body; // z.B. {kundenNummer: 1, betrag: 5.50, pin: 1111}
	let cid = json.Input.cid;
	let pin = json.Input.pin;
	let priceToPay = json.Input.price;
	console.log("PAY REQUEST: " + JSON.stringify(json));
	let tmpKonten = konten.filter(k => k.cid === cid);
	if (tmpKonten.length !== 1) {
		res.json({result: false, message: "Kundennummer existiert nicht"});
	} else {
		let konto = tmpKonten[0];
		if (konto.pin !== pin) {
			res.json({result: false, message: "Falsche PIN!"});
		} else {
			if (konto.budget < priceToPay) {
				res.json({result: false, message: "Nicht genügend Geld auf dem Konto!"});
			} else {
				konto.budget = konto.budget - priceToPay;
				res.json({result: true, message: "Bezahlung erfolgreich"});
			}
		}
	}
});


app.listen(3000);
