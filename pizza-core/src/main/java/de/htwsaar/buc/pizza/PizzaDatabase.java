package de.htwsaar.buc.pizza;

import java.util.List;

/**
 *
 * @author jsteuer
 */
public interface PizzaDatabase {

	public List<Pizza> getAvailablePizzas();

}
