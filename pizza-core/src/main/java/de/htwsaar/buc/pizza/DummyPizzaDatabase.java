package de.htwsaar.buc.pizza;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jsteuer
 */
public class DummyPizzaDatabase implements PizzaDatabase {

	@Override
	public List<Pizza> getAvailablePizzas() {
		return Arrays.asList(
				new Pizza(10, "Margherita", 500),
				new Pizza(11, "Salami", 550),
				new Pizza(18, "Hawaii", 600),
				new Pizza(27, "Sophia Loren", 550)
		);
	}

}
