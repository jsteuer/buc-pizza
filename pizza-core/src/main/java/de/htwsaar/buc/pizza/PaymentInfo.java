package de.htwsaar.buc.pizza;

/**
 *
 * @author jsteuer
 */
public class PaymentInfo {

	private final String customerId;
	private final String pin;
	private final String discountCode;

	public PaymentInfo(String customerId, String pin, String discountCode) {
		this.customerId = customerId;
		this.pin = pin;
		this.discountCode = discountCode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public String getPin() {
		return pin;
	}

	public String getDiscountCode() {
		return discountCode;
	}

}
