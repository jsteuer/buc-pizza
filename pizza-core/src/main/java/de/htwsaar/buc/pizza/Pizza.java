package de.htwsaar.buc.pizza;

/**
 *
 * @author jsteuer
 */
public class Pizza {

	private final int id;
	private final String name;
	private final int priceCents;

	public Pizza(int id, String name, int priceCents) {
		this.id = id;
		this.name = name;
		this.priceCents = priceCents;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getPriceCents() {
		return priceCents;
	}

}
