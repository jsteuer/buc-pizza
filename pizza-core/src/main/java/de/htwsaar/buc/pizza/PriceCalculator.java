package de.htwsaar.buc.pizza;

import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

/**
 *
 * @author jsteuer
 */
public class PriceCalculator {

	private final PizzaDatabase pizzaDatabase;

	@Inject
	public PriceCalculator(PizzaDatabase pizzaDatabase) {
		this.pizzaDatabase = pizzaDatabase;
	}

	/**
	 * Berechnet den Preis in Cent
	 *
	 * @param pizzaIds
	 * @param pizzaDatabase
	 * @return
	 */
	public int calculatePrice(List<Integer> pizzaIds) {
		List<Pizza> pizzas = pizzaDatabase.getAvailablePizzas();
		int price = 0;
		for (int pizzaId : pizzaIds) {
			Optional<Pizza> optPizza = pizzas.stream()
					.filter(p -> p.getId() == pizzaId)
					.findAny();
			if (optPizza.isPresent()) {
				price += optPizza.get().getPriceCents();
			} else {
				throw new RuntimeException("Pizza not found, id=" + pizzaId);
			}
		}
		return price;
	}

}
