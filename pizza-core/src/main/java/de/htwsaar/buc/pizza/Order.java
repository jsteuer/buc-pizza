package de.htwsaar.buc.pizza;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jsteuer
 */
public class Order {

	private final List<Integer> pizzaIds = new LinkedList();
	private final PaymentInfo paymentInfo;

	public Order(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public void addPizza(int pizzaId) {
		this.pizzaIds.add(pizzaId);
	}

	public List<Integer> getPizzaIds() {
		return Collections.unmodifiableList(this.pizzaIds);
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

}
