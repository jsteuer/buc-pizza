package de.htwsaar.buc.pizzaweb;

import de.htwsaar.buc.pizza.DummyPizzaDatabase;
import de.htwsaar.buc.pizza.PizzaDatabase;
import de.htwsaar.buc.pizza.PriceCalculator;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.camunda.bpm.ProcessEngineService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import java.util.logging.Logger;
import javax.ws.rs.ApplicationPath;

/**
 * @author jsteuer
 */
@ApplicationPath("api")
public class Application extends ResourceConfig {

	private final static Logger LOGGER = Logger.getLogger(Application.class.getSimpleName());

	public Application() {

		// Features
		register(JacksonFeature.class);

		// DI
		register(new AbstractBinder() {
			@Override
			protected void configure() {
				bind(lookupProcessEngineService()).to(ProcessEngineService.class);
				bind(DummyPizzaDatabase.class).to(PizzaDatabase.class);
				bind(PriceCalculator.class).to(PriceCalculator.class);
			}
		});

		// Endpoints
		register(EndpointPizzaService.class);

	}

	private ProcessEngineService lookupProcessEngineService() {
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ProcessEngineService processEngine = (ProcessEngineService) envContext.lookup("ProcessEngineService");
			return processEngine;
		} catch (NamingException e) {
			throw new RuntimeException("can not lookup camunda process engine service: " + e.getMessage(), e);
		}
	}

}
