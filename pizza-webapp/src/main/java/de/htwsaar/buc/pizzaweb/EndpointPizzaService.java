package de.htwsaar.buc.pizzaweb;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.htwsaar.buc.pizza.Order;
import de.htwsaar.buc.pizza.PaymentInfo;
import de.htwsaar.buc.pizza.Pizza;
import de.htwsaar.buc.pizza.PizzaDatabase;
import de.htwsaar.buc.pizza.PriceCalculator;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.camunda.bpm.ProcessEngineService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;

/**
 *
 * @author jsteuer
 */
@Path("")
public class EndpointPizzaService {

	private final ProcessEngineService processEngineService;
	private final PizzaDatabase pizzaDatabase;
	private final PriceCalculator priceCalculator;

	@Inject
	public EndpointPizzaService(
			ProcessEngineService processEngineService,
			PizzaDatabase pizzaDatabase,
			PriceCalculator priceCalculator
	) {
		this.processEngineService = processEngineService;
		this.pizzaDatabase = pizzaDatabase;
		this.priceCalculator = priceCalculator;
	}

	@GET
	@Path("pizzas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAvailablePizzas() {
		List<Pizza> pizzas = pizzaDatabase.getAvailablePizzas();
		return Response.ok(pizzas).build();
	}

	@POST
	@Path("orders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response orderPizza(
			ObjectNode payload
	) {
		ProcessEngine engine = processEngineService.getDefaultProcessEngine();
		RuntimeService runtimeService = engine.getRuntimeService();
		TaskService taskService = engine.getTaskService();

		// parse and verify data from client
		Order order = parseOrder(payload);
		PaymentInfo paymentInfo = order.getPaymentInfo();
		priceCalculator.calculatePrice(order.getPizzaIds()); // verification

		// start a new process
		ProcessInstance processInstance = runtimeService
				.startProcessInstanceByKey("pizza-bestellung");

		// forward informations to the current task (collect informations about the order)
		Task task = taskService.createTaskQuery()
				.processInstanceId(processInstance.getProcessInstanceId())
				.singleResult();

		String piid = processInstance.getId();
		runtimeService.setVariable(piid, "paymentInfo.customerId", paymentInfo.getCustomerId());
		runtimeService.setVariable(piid, "paymentInfo.discountCode", paymentInfo.getDiscountCode());
		runtimeService.setVariable(piid, "paymentInfo.pin", paymentInfo.getPin());
		runtimeService.setVariable(piid, "pizzaIds", order.getPizzaIds().stream().map(id -> Integer.toString(id)).collect(Collectors.joining(",")));
		
		// complete the task. engine will lookup subsequent task
		taskService.complete(task.getId());
		
		// send response to the client
		return Response.ok("{}").build();
	}

	private Order parseOrder(JsonNode payload) {
		PaymentInfo paymentInfo = new PaymentInfo(
				payload.get("paymentInfo").get("cid").asText(),
				payload.get("paymentInfo").get("pin").asText(),
				payload.get("paymentInfo").get("discountCode").asText()
		);
		Order order = new Order(paymentInfo);

		Arrays.stream(payload.get("pizzaIds").asText().split(","))
				.map(Integer::parseInt)
				.forEach(order::addPizza);

		return order;
	}

}
